PRAGMA foreign_keys = ON;
DROP TABLE IF EXISTS "Badge";
CREATE TABLE [Badge] (
  [Id] nchar(10) NOT NULL
, [Description] ntext NOT NULL
, [Value] int NOT NULL
, [CreatorId] nvarchar(20) DEFAULT 'System' NULL
, [DateCreated] datetime NOT NULL
, [DateExpired] datetime NULL
, [IsValid] bit DEFAULT 0 NOT NULL
, CONSTRAINT [PK__tmp_ms_x__3214EC06C5529F07] PRIMARY KEY ([Id])
, FOREIGN KEY ([CreatorId]) REFERENCES [User] ([Login]) ON DELETE SET DEFAULT ON UPDATE CASCADE
);
INSERT INTO "Badge" VALUES('4XE6C5V6T7','Test 1 badge',10,'Admin','2005-05-05 00:00:00.000',NULL,1);
INSERT INTO "Badge" VALUES('4XEC5RFV6T','User badge',-20,'User','2007-05-02 00:00:00.000',NULL,0);
INSERT INTO "Badge" VALUES('X5E46C5R7V','Test 2 badge',40,'Admin','2005-03-06 00:00:00.000','2005-07-03 00:00:00.000',1);
DROP TABLE IF EXISTS "Config";
CREATE TABLE [Config] (
  [Name] nvarchar(20) NOT NULL
, [IsEnabled] bit NOT NULL
, CONSTRAINT [PK__Config__737584F7257456B7] PRIMARY KEY ([Name])
);
INSERT INTO "Config" VALUES('Config1',1);
INSERT INTO "Config" VALUES('Config2',0);
INSERT INTO "Config" VALUES('Config3',0);
INSERT INTO "Config" VALUES('Config4',0);
INSERT INTO "Config" VALUES('Config5',1);
INSERT INTO "Config" VALUES('AllowGuestLogin',1);
DROP TABLE IF EXISTS "Password";
CREATE TABLE [Password] (
  [UserId] nvarchar(20) NOT NULL
, [Age] int NOT NULL
, [Value] nvarchar(20) NOT NULL
, CONSTRAINT [PK__Password__0BE1BDE8904C0C07] PRIMARY KEY ([UserId],[Age])
, FOREIGN KEY ([UserId]) REFERENCES [User] ([Login]) ON DELETE CASCADE ON UPDATE CASCADE
);
INSERT INTO "Password" VALUES('Admin',0,'Admin');
INSERT INTO "Password" VALUES('Admin',1,'Admin1');
INSERT INTO "Password" VALUES('Admin',2,'Admin2');
INSERT INTO "Password" VALUES('Root',0,'Root');
INSERT INTO "Password" VALUES('User',0,'User');
DROP TABLE IF EXISTS "Permission";
CREATE TABLE [Permission] (
  [Id] int NOT NULL
, [Name] nvarchar(20) NOT NULL
, CONSTRAINT [PK__Permissi__3214EC07090FD36F] PRIMARY KEY ([Id])
);
INSERT INTO "Permission" VALUES(1,'Guest');
INSERT INTO "Permission" VALUES(2,'User');
INSERT INTO "Permission" VALUES(3,'SpecialUser');
INSERT INTO "Permission" VALUES(4,'Moderator');
INSERT INTO "Permission" VALUES(5,'Admin');
DROP TABLE IF EXISTS "RequiredPermission";
CREATE TABLE [RequiredPermission] (
  [TabName] nvarchar(20) NOT NULL
, [PermissionId] int NOT NULL
, CONSTRAINT [PK__tmp_ms_x__2BCF0768FB98AD1A] PRIMARY KEY ([TabName])
, FOREIGN KEY ([PermissionId]) REFERENCES [Permission] ([Id]) ON DELETE SET DEFAULT ON UPDATE CASCADE
);
INSERT INTO "RequiredPermission" VALUES('Config',1);
INSERT INTO "RequiredPermission" VALUES('Login',4);
INSERT INTO "RequiredPermission" VALUES('Setting',5);
INSERT INTO "RequiredPermission" VALUES('Stats',2);
INSERT INTO "RequiredPermission" VALUES('Assign badges',4);
INSERT INTO "RequiredPermission" VALUES('Welcome',1);
INSERT INTO "RequiredPermission" VALUES('Statistics',1);
INSERT INTO "RequiredPermission" VALUES('Browse users',1);
INSERT INTO "RequiredPermission" VALUES('Browse badges',1);
INSERT INTO "RequiredPermission" VALUES('Admin panel',4);
INSERT INTO "RequiredPermission" VALUES('Manage users',4);
INSERT INTO "RequiredPermission" VALUES('Manage badges',4);
INSERT INTO "RequiredPermission" VALUES('Edit config',5);
DROP TABLE IF EXISTS "User";
CREATE TABLE [User] (
  [FirstName] nvarchar(20) NOT NULL
, [LastName] nvarchar(20) NOT NULL
, [Login] nvarchar(20) NOT NULL
, [Email] nvarchar(50) NOT NULL
, [DateJoined] datetime NOT NULL
, [IsApproved] bit DEFAULT 0 NOT NULL
, [PermissionId] int DEFAULT 1 NOT NULL
, CONSTRAINT [PK__tmp_ms_x__5E55825A3860485C] PRIMARY KEY ([Login])
, FOREIGN KEY ([PermissionId]) REFERENCES [Permission] ([Id]) ON DELETE SET DEFAULT ON UPDATE CASCADE
);
INSERT INTO "User" VALUES('Admin','Admin','Admin','Admin','2000-01-01 00:00:00.000',1,5);
INSERT INTO "User" VALUES('Guest','Guest','Guest','Guest','2012-01-01 00:00:00.000',0,1);
INSERT INTO "User" VALUES('Root','Root','Root','Root','1999-12-12 00:00:00.000',1,4);
INSERT INTO "User" VALUES('System','System','System','System','1970-01-01 00:00:00.000',1,5);
INSERT INTO "User" VALUES('User','User','User','User','2022-12-12 00:00:00.000',1,2);
DROP TABLE IF EXISTS "User_Badge";
CREATE TABLE [User_Badge] (
  [UserLogin] nvarchar(20) NOT NULL
, [BadgeId] nchar(10) NOT NULL
, [DateEarned] datetime NOT NULL
, [GivenBy] nvarchar(20) DEFAULT 'System' NOT NULL
, CONSTRAINT [PK__tmp_ms_x__AE1F0F6A86598113] PRIMARY KEY ([UserLogin],[BadgeId])
, FOREIGN KEY ([GivenBy]) REFERENCES [User] ([Login]) ON DELETE SET DEFAULT ON UPDATE CASCADE
, FOREIGN KEY ([UserLogin]) REFERENCES [User] ([Login]) ON DELETE CASCADE ON UPDATE CASCADE
, FOREIGN KEY ([BadgeId]) REFERENCES [Badge] ([Id]) ON DELETE CASCADE ON UPDATE CASCADE
);
INSERT INTO "User_Badge" VALUES('Admin','4XE6C5V6T7','2000-01-01 00:00:00.000','System');
INSERT INTO "User_Badge" VALUES('Admin','X5E46C5R7V','2005-03-01 00:00:00.000','System');
INSERT INTO "User_Badge" VALUES('Root','4XE6C5V6T7','2001-01-01 00:00:00.000','System');
INSERT INTO "User_Badge" VALUES('User','4XE6C5V6T7','2002-01-01 00:00:00.000','Admin');
CREATE TRIGGER "NewBadge" AFTER INSERT ON "Badge" FOR EACH ROW  BEGIN 
UPDATE Badge SET DateCreated = CURRENT_TIMESTAMP where Id = NEW.Id;
END;
CREATE TRIGGER "NewEarned" AFTER INSERT ON "User_Badge" FOR EACH ROW  BEGIN 
UPDATE User_Badge SET DateEarned = CURRENT_TIMESTAMP 
where UserLogin = NEW.UserLogin and BadgeId = NEW.BadgeId; 
END;
CREATE TRIGGER "NewPassword" BEFORE INSERT ON "Password" BEGIN 
UPDATE Password SET Age = -(Age + 1) WHERE UserId = NEW.UserId;
UPDATE Password SET Age = -Age WHERE UserId = NEW.UserId;
END;
CREATE TRIGGER "NewUser" AFTER INSERT ON "User" FOR EACH ROW  BEGIN 
UPDATE User SET DateJoined = CURRENT_TIMESTAMP where Login =NEW.Login; 
END;
CREATE INDEX [IX_Badge_Creator_Badge] ON [Badge] ([CreatorId] ASC);
CREATE INDEX [IX_User_Badge_GivenBy_User_Badge] ON [User_Badge] ([GivenBy] ASC);
CREATE INDEX [IX_User_FullName_User] ON [User] ([FirstName] ASC,[LastName] ASC);
CREATE UNIQUE INDEX [IX_User_Login_User] ON [User] ([Login] ASC);
