PRAGMA foreign_keys = ON;
DROP TABLE IF EXISTS "Badge";
CREATE TABLE [Badge] (
  [Id] nchar(10) NOT NULL
, [Description] ntext NOT NULL
, [Value] int NOT NULL
, [CreatorId] nvarchar(20) DEFAULT 'System' NULL
, [DateCreated] datetime NOT NULL
, [DateExpired] datetime NULL
, [IsValid] bit DEFAULT 0 NOT NULL
, CONSTRAINT [PK__tmp_ms_x__3214EC06C5529F07] PRIMARY KEY ([Id])
, FOREIGN KEY ([CreatorId]) REFERENCES [User] ([Login]) ON DELETE SET DEFAULT ON UPDATE CASCADE
);
INSERT INTO "Badge" VALUES('ZAMGppy78M','Rejestracja',10,'Admin','2005-05-05 00:00:00',NULL,1);
INSERT INTO "Badge" VALUES('KvMnh8iuPZ','Złośliwość stwórcy',-20,'Cthulu','2007-05-02 00:00:00',NULL,0);
INSERT INTO "Badge" VALUES('ngxoPlvoVi','Zrobienie fikołka',40,'Admin','2005-03-06 00:00:00','2005-07-03 00:00:00',1);
INSERT INTO "Badge" VALUES('sdm1RQvYkc','Postawienie piwa',80,'Root','2008-03-06 00:00:00','2012-07-03 00:00:00',1);
INSERT INTO "Badge" VALUES('73B9FmuWHc','Napisanie mądrego artykułu',20,'MASH','2013-03-06 00:00:00','2015-07-03 00:00:00',1);
DROP TABLE IF EXISTS "Config";
CREATE TABLE [Config] (
  [Name] nvarchar(20) NOT NULL
, [IsEnabled] bit NOT NULL
, CONSTRAINT [PK__Config__737584F7257456B7] PRIMARY KEY ([Name])
);
INSERT INTO "Config" VALUES('AutoApproveNewUsers',0);
INSERT INTO "Config" VALUES('AllowUserCreatingBadges',1);
INSERT INTO "Config" VALUES('AllowToLogin',1);
INSERT INTO "Config" VALUES('AllowGuestLogin',0);
DROP TABLE IF EXISTS "Password";
CREATE TABLE [Password] (
  [UserId] nvarchar(20) NOT NULL
, [Age] int NOT NULL
, [Value] nvarchar(20) NOT NULL
, CONSTRAINT [PK__Password__0BE1BDE8904C0C07] PRIMARY KEY ([UserId],[Age])
, FOREIGN KEY ([UserId]) REFERENCES [User] ([Login]) ON DELETE CASCADE ON UPDATE CASCADE
);
INSERT INTO "Password" VALUES('Admin' ,0,'Admin');
INSERT INTO "Password" VALUES('Admin' ,1,'Admin1');
INSERT INTO "Password" VALUES('Admin' ,2,'Admin2');
INSERT INTO "Password" VALUES('Root'  ,0,'Root');
INSERT INTO "Password" VALUES('User'  ,0,'User');
INSERT INTO "Password" VALUES('Cthulu',0,'Doom');
INSERT INTO "Password" VALUES('MASH'  ,0,'Hawkeye');
DROP TABLE IF EXISTS "Permission";
CREATE TABLE [Permission] (
  [Id] int NOT NULL
, [Name] nvarchar(20) NOT NULL
, CONSTRAINT [PK__Permissi__3214EC07090FD36F] PRIMARY KEY ([Id])
);
INSERT INTO "Permission" VALUES(1,'Guest');
INSERT INTO "Permission" VALUES(2,'User');
INSERT INTO "Permission" VALUES(3,'Moderator');
INSERT INTO "Permission" VALUES(4,'Admin');
INSERT INTO "Permission" VALUES(5,'Root');
DROP TABLE IF EXISTS "RequiredPermission";
CREATE TABLE [RequiredPermission] (
  [TabName] nvarchar(20) NOT NULL
, [PermissionId] int NOT NULL
, CONSTRAINT [PK__tmp_ms_x__2BCF0768FB98AD1A] PRIMARY KEY ([TabName])
, FOREIGN KEY ([PermissionId]) REFERENCES [Permission] ([Id]) ON DELETE SET DEFAULT ON UPDATE CASCADE
);
INSERT INTO "RequiredPermission" VALUES('Config',1);
INSERT INTO "RequiredPermission" VALUES('Login',4);
INSERT INTO "RequiredPermission" VALUES('Setting',5);
INSERT INTO "RequiredPermission" VALUES('Stats',2);
INSERT INTO "RequiredPermission" VALUES('Assign badges',4);
INSERT INTO "RequiredPermission" VALUES('Welcome',1);
INSERT INTO "RequiredPermission" VALUES('Statistics',1);
INSERT INTO "RequiredPermission" VALUES('Browse users',1);
INSERT INTO "RequiredPermission" VALUES('Browse badges',1);
INSERT INTO "RequiredPermission" VALUES('Admin panel',4);
INSERT INTO "RequiredPermission" VALUES('Manage users',4);
INSERT INTO "RequiredPermission" VALUES('Manage badges',4);
INSERT INTO "RequiredPermission" VALUES('Edit config',5);
DROP TABLE IF EXISTS "User";
CREATE TABLE [User] (
  [FirstName] nvarchar(20) NOT NULL
, [LastName] nvarchar(20) NOT NULL
, [Login] nvarchar(20) NOT NULL
, [Email] nvarchar(50) NOT NULL
, [DateJoined] datetime NOT NULL
, [IsApproved] bit DEFAULT 0 NOT NULL
, [PermissionId] int DEFAULT 1 NOT NULL
, CONSTRAINT [PK__tmp_ms_x__5E55825A3860485C] PRIMARY KEY ([Login])
, FOREIGN KEY ([PermissionId]) REFERENCES [Permission] ([Id]) ON DELETE SET DEFAULT ON UPDATE CASCADE
);
INSERT INTO "User" VALUES('Jan','Brzechwa','Admin','brzechwaj@stragan.pl','2000-01-01 00:00:00',1,5);
INSERT INTO "User" VALUES('Teodor','Puc','Guest','noreply@gsystem.net','2012-01-01 00:00:00',0,1);
INSERT INTO "User" VALUES('Root','Root','Root','root@root.woot','1999-12-12 00:00:00',1,5);
INSERT INTO "User" VALUES('System','System','System','','1970-01-01 00:00:00',1,5);
INSERT INTO "User" VALUES('Q’thulu','Thu Thu','Cthulu','cthulu@rlyehian.hp','2002-12-12 00:00:00',1,2);
INSERT INTO "User" VALUES('Ewa','Bielska','User','ewa.biel@ska.org','2012-12-12 00:00:00',1,1);
INSERT INTO "User" VALUES('Radar','Nguyen','MASH','4077@pierce.sk','2001-03-09 18:54:00',1,3);
DROP TABLE IF EXISTS "User_Badge";
CREATE TABLE [User_Badge] (
  [UserLogin] nvarchar(20) NOT NULL
, [BadgeId] nchar(10) NOT NULL
, [DateEarned] datetime NOT NULL
, [GivenBy] nvarchar(20) DEFAULT 'System' NOT NULL
, CONSTRAINT [PK__tmp_ms_x__AE1F0F6A86598113] PRIMARY KEY ([UserLogin],[BadgeId])
, FOREIGN KEY ([GivenBy]) REFERENCES [User] ([Login]) ON DELETE SET DEFAULT ON UPDATE CASCADE
, FOREIGN KEY ([UserLogin]) REFERENCES [User] ([Login]) ON DELETE CASCADE ON UPDATE CASCADE
, FOREIGN KEY ([BadgeId]) REFERENCES [Badge] ([Id]) ON DELETE CASCADE ON UPDATE CASCADE
);
INSERT INTO "User_Badge" VALUES('Admin' ,'ZAMGppy78M','2000-01-01 00:00:00','Root');
INSERT INTO "User_Badge" VALUES('Admin' ,'KvMnh8iuPZ','2005-03-01 00:00:00','System');
INSERT INTO "User_Badge" VALUES('Root'  ,'ngxoPlvoVi','2001-01-01 00:00:00','Admin');
INSERT INTO "User_Badge" VALUES('Cthulu'  ,'ngxoPlvoVi','2011-01-01 00:00:00','Admin');
INSERT INTO "User_Badge" VALUES('System','sdm1RQvYkc','2015-11-12 21:50:56','System');
INSERT INTO "User_Badge" VALUES('Admin' ,'73B9FmuWHc','2015-11-12 21:50:59','System');
INSERT INTO "User_Badge" VALUES('Root'  ,'73B9FmuWHc','2015-11-12 21:50:59','System');
INSERT INTO "User_Badge" VALUES('User'  ,'73B9FmuWHc','2015-11-12 21:50:59','System');
INSERT INTO "User_Badge" VALUES('Cthulu','73B9FmuWHc','2015-11-12 21:50:59','System');
INSERT INTO "User_Badge" VALUES('MASH'  ,'73B9FmuWHc','2015-11-12 21:50:59','System');
CREATE TRIGGER "NewBadge" AFTER INSERT ON "Badge" FOR EACH ROW  BEGIN 
UPDATE Badge SET DateCreated = CURRENT_TIMESTAMP where Id = NEW.Id;
END;
CREATE TRIGGER "NewEarned" AFTER INSERT ON "User_Badge" FOR EACH ROW  BEGIN 
UPDATE User_Badge SET DateEarned = CURRENT_TIMESTAMP 
where UserLogin = NEW.UserLogin and BadgeId = NEW.BadgeId; 
END;
CREATE TRIGGER "NewPassword" BEFORE INSERT ON "Password" BEGIN 
UPDATE Password SET Age = -(Age + 1) WHERE UserId = NEW.UserId;
UPDATE Password SET Age = -Age WHERE UserId = NEW.UserId;
END;
CREATE TRIGGER "NewUser" AFTER INSERT ON "User" FOR EACH ROW  BEGIN 
UPDATE User SET DateJoined = CURRENT_TIMESTAMP where Login =NEW.Login; 
END;
CREATE INDEX [IX_Badge_Creator_Badge] ON [Badge] ([CreatorId] ASC);
CREATE INDEX [IX_User_Badge_GivenBy_User_Badge] ON [User_Badge] ([GivenBy] ASC);
CREATE INDEX [IX_User_FullName_User] ON [User] ([FirstName] ASC,[LastName] ASC);
CREATE UNIQUE INDEX [IX_User_Login_User] ON [User] ([Login] ASC);
